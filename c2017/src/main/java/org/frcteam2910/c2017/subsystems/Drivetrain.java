package org.frcteam2910.c2017.subsystems;

import edu.wpi.first.wpilibj.*;
import org.frcteam2910.c2017.OI;
import org.frcteam2910.c2017.RobotMap;
import org.frcteam2910.common.drivers.Gyroscope;
import org.frcteam2910.common.robot.commands.CheesyDriveCommand;
import org.frcteam2910.common.robot.drivers.NavX;
import org.frcteam2910.common.robot.subsystems.ShiftingTankDrivetrain;

public final class Drivetrain extends ShiftingTankDrivetrain {
	public static final double TICKS_PER_INCH = 1.0; // TODO: Calibrate ticks per inch

	private static final Drivetrain INSTANCE = new Drivetrain();

	private final DoubleSolenoid shiftingSolenoid = new DoubleSolenoid(
			RobotMap.DRIVETRAIN_SHIFTER[0],
			RobotMap.DRIVETRAIN_SHIFTER[1]
	);

	private final SpeedController leftMaster;
	private final Encoder leftEncoder;

	private final SpeedController rightMaster;
	private final Encoder rightEncoder;

	private final NavX navX = new NavX(SPI.Port.kMXP);

	private Drivetrain() {
		super(24);

		SpeedController[] leftSlaves = new SpeedController[RobotMap.DRIVETRAIN_LEFT_MOTORS.length - 1];
		for (int i = 1; i < leftSlaves.length; i++)
			leftSlaves[i - 1] = new VictorSP(RobotMap.DRIVETRAIN_LEFT_MOTORS[i]);
		leftMaster = new SpeedControllerGroup(new VictorSP(RobotMap.DRIVETRAIN_LEFT_MOTORS[0]), leftSlaves);
		leftMaster.setInverted(true);

		leftEncoder = new Encoder(RobotMap.DRIVETRAIN_LEFT_ENCODER[0], RobotMap.DRIVETRAIN_LEFT_ENCODER[1]);
		leftEncoder.setDistancePerPulse(1.0 / TICKS_PER_INCH);


		SpeedController[] rightSlaves = new SpeedController[RobotMap.DRIVETRAIN_RIGHT_MOTORS.length - 1];
		for (int i = 1; i < rightSlaves.length; i++)
			rightSlaves[i - 1] = new VictorSP(RobotMap.DRIVETRAIN_RIGHT_MOTORS[i]);
		rightMaster = new SpeedControllerGroup(new VictorSP(RobotMap.DRIVETRAIN_RIGHT_MOTORS[0]), rightSlaves);

		rightEncoder = new Encoder(RobotMap.DRIVETRAIN_RIGHT_ENCODER[0], RobotMap.DRIVETRAIN_RIGHT_ENCODER[1]);
		rightEncoder.setDistancePerPulse(1.0 / TICKS_PER_INCH);
	}

	public static Drivetrain getInstance() {
		return INSTANCE;
	}

	@Override
	public boolean inHighGear() {
		return shiftingSolenoid.get() == DoubleSolenoid.Value.kForward;
	}

	@Override
	public void setHighGear(boolean highGear) {
		shiftingSolenoid.set(highGear ? DoubleSolenoid.Value.kForward : DoubleSolenoid.Value.kReverse);
	}

	@Override
	public double getLeftDistance() {
		return leftEncoder.getDistance();
	}

	@Override
	public double getRightDistance() {
		return rightEncoder.getDistance();
	}

	@Override
	public void tankDrive(double left, double right) {
		leftMaster.set(left);
		rightMaster.set(right);
	}

	@Override
	public Gyroscope getGyroscope() {
		return navX;
	}

	@Override
	public double getMaximumVelocity() {
		return 15.0;
	}

	@Override
	public double getMaximumAcceleration() {
		return 5.0;
	}

	@Override
	protected void initDefaultCommand() {
		setDefaultCommand(new CheesyDriveCommand(this,
				OI.getInstance().getDrivetrainThrottleAxis(),
				OI.getInstance().getDrivetrainTurnAxis(),
				OI.getInstance().getDrivetrainQuickTurnButton()));
	}
}
