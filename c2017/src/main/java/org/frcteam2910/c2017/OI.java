package org.frcteam2910.c2017;

import edu.wpi.first.wpilibj.buttons.Button;
import org.frcteam2910.c2017.subsystems.Drivetrain;
import org.frcteam2910.common.robot.commands.SetDrivetrainGearCommand;
import org.frcteam2910.common.robot.input.Axis;
import org.frcteam2910.common.robot.input.Controller;
import org.frcteam2910.common.robot.input.XboxController;

public class OI {
	private static final OI INSTANCE = new OI();

	private Controller controller = new XboxController(0);

	private OI() { }

	public void bindControls() {
		getDrivetrainSetLowGearButton().whenPressed(new SetDrivetrainGearCommand(
				Drivetrain.getInstance(),
				false
		));
		getDrivetrainSetHighGearButton().whenPressed(new SetDrivetrainGearCommand(
				Drivetrain.getInstance(),
				true
		));
	}

	public static OI getInstance() {
		return INSTANCE;
	}

	public Axis getDrivetrainThrottleAxis() {
		return controller.getLeftYAxis();
	}

	public Axis getDrivetrainTurnAxis() {
		return controller.getRightXAxis();
	}

	public Button getDrivetrainQuickTurnButton() {
		return controller.getLeftJoystickButton();
	}

	public Button getDrivetrainSetHighGearButton() {
		return controller.getRightBumperButton();
	}

	public Button getDrivetrainSetLowGearButton() {
		return controller.getLeftBumperButton();
	}
}
